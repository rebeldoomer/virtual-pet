import random
import pygame
import json
import os
from datetime import datetime

# Initialize Pygame and Game constants
pygame.init()
SCREEN_WIDTH, SCREEN_HEIGHT, FPS = 800, 600, 60
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Virtual E-Pet!')
clock = pygame.time.Clock()

# Load sprites
# Initialize a dictionary to hold the sprites
sprites = {}

# Assuming all sprite images are in the 'sprites/' folder and are PNG files
sprite_files = {
    "Chinchilla": "Chinchilla.png",
    "Chipmunk": "Chipmunk.png",
    "Ferret": "Ferret.png",
    "Gerbil": "Gerbil.png",
    "GuineaPig": "GuineaPig.png",
    "Hamster": "Hamster.png",
    "Hedgehog": "Hedgehog.png",
    "Mouse": "Mouse.png",
    "Squirrel": "Squirrel.png",
    "rip": "rip.png",
}

# Loop over the sprite_files dictionary and load each sprite
for name, file in sprite_files.items():
    path = os.path.join('sprites', file)  # Construct the path to the sprite file
    try:
        sprites[name] = pygame.image.load(path).convert_alpha()  # Load and convert the image
    except pygame.error as e:
        print(f"Failed to load sprite '{name}' from '{path}': {e}")


# Initialize a dictionary to hold the sounds
sounds = {}

# Assuming all your sound files are in the 'sounds/' folder and are MP3 files
sound_files = {
    "death": "death-sound.mp3",
    "game_start": "game-start-6104.mp3",
    "gamewin": "gamewin.mp3",
    "hunger_notification": "hungernotif.mp3",
    "level_up": "level-up.mp3",
    "lose_game": "losegame.mp3",
}

# Loop over the sound_files dictionary and load each sound
for name, file in sound_files.items():
    path = os.path.join('sounds', file)  # Construct the path to the sound file
    try:
        sounds[name] = pygame.mixer.Sound(path)  # Load the sound
    except pygame.error as e:
        print(f"Failed to load sound '{name}' from '{path}': {e}")

# Game States
MAIN_MENU = 0
SELECT_PET = 1
GAME_PLAY = 2
game_state = MAIN_MENU  # Start game at main menu

# todo FUNCTION DEFINITIONS GO HERE!!


# def main_menu():

def select_pet_screen():
    # Here, you decide whether to use the slot machine or dice roll
    # For simplicity, let's say the player presses "S" for slot machine and "D" for dice roll
    keys = pygame.key.get_pressed()
    if keys[pygame.K_s]:
        return slot_machine_select_pet()
    elif keys[pygame.K_d]:
        return dice_roll_select_pet()
    return None


def game_play(pet):
    # Main game logic after pet selection
    # This function should handle all the game logic once the pet has been selected
    pass


running = True
selected_pet = None
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    if game_state == MAIN_MENU:
        main_menu()  # Display main menu
        # Placeholder: Press "N" to start new game (and go to pet selection)
        keys = pygame.key.get_pressed()
        if keys[pygame.K_n]:
            game_state = SELECT_PET

    elif game_state == SELECT_PET:
        selected_pet = select_pet_screen()  # Select pet process
        if selected_pet:
            game_state = GAME_PLAY  # Move to game play state after selecting a pet

    elif game_state == GAME_PLAY:
        game_play(selected_pet)  # Main game play

    pygame.display.flip()
    clock.tick(FPS)

# Dictionary mapping pet names to their rarity
pet_rarities = {
    "Squirrel": "Rare",
    "Chipmunk": "Rare",
    "Hamster": "Common",
    "Chinchilla": "Uncommon",
    "Ferret": "Uncommon",
    "Gerbil": "Uncommon",
    "GuineaPig": "Uncommon",
    "Hedgehog": "Uncommon",
    "Mouse": "Uncommon",
    # Add new pets from the uploaded image
    "Rip": "Uncommon",  # Assuming 'Rip' is a placeholder for a tombstone image, adjust as necessary
}

# Define probabilities for each rarity level
rarity_probabilities = {
    "Common": 0.5,  # 50% chance to get a common pet
    "Uncommon": 0.3,  # 30% chance for uncommon
    "Rare": 0.2,  # 20% chance for rare
}


# Function to randomly select a pet based on rarity
def select_pet_by_rarity():
    # Generate a random number between 0 and 1
    rand = random.random()

    # Determine rarity based on probabilities
    cumulative_probability = 0.0
    for rarity, probability in rarity_probabilities.items():
        cumulative_probability += probability
        if rand < cumulative_probability:
            # Select a pet from this rarity category
            pets_in_rarity = [pet for pet, pet_rarity in pet_rarities.items() if pet_rarity == rarity]
            return random.choice(pets_in_rarity), rarity
    # In case the random number is very close to 1 and doesn't get picked up in the loop
    return "Hamster", "Common"


# Slot machine logic to select a pet with a chance for rarity
def slot_machine_select_pet():
    # This function simulates a slot machine that selects a pet based on rarity
    pets = list(pet_rarities.keys())
    weights = [rarity_probabilities[pet_rarities[pet]] for pet in pets]

    # Spin the slot machine three times
    slot_results = random.choices(pets, weights, k=3)

    # Check if all three results are the same
    if slot_results.count(slot_results[0]) == 3:
        selected_pet = slot_results[0]
        pet_rarity = pet_rarities[selected_pet]
        sounds['game_start'].play()
        print(f"Congratulations! You got a {pet_rarity} pet: {selected_pet}")
        return selected_pet
    else:
        print("No match! Try again!")
        return None

def dice_roll_select_pet():
    # This function simulates a dice roll to select a pet based on rarity
    # Each die corresponds to a rarity level
    die_roll = random.randint(1, 6)  # Roll a six-sided die

    # Assign rarity based on the die roll
    if die_roll in [1, 2]:  # 1-2 for common pets
        rarity = "Common"
    elif die_roll in [3, 4]:  # 3-4 for uncommon pets
        rarity = "Uncommon"
    else:  # 5-6 for rare pets
        rarity = "Rare"

    # Select a pet from the rolled rarity
    pets_in_rarity = [pet for pet, pet_rarity in pet_rarities.items() if pet_rarity == rarity]
    selected_pet = random.choice(pets_in_rarity)
    sounds['game_start'].play()
    print(f"You rolled a {die_roll} and got a {rarity} pet: {selected_pet}")
    return selected_pet

# Add a game loop event where the player chooses how to select their pet
# ...

# Example usage in the game loop
while running:
    # ... other game loop code ...

    # Let's assume the player has chosen to select their pet
    # You would typically have this as part of your UI interaction
    if player_choice == 'slot_machine':
        pet = slot_machine_select_pet()
    elif player_choice == 'dice_roll':
        pet = dice_roll_select_pet()

    # If a pet has been selected, set up its properties
    if pet:
        pet_type = pet
        hunger_bar = 100  # Full hunger bar
        # ... Set up other pet properties ...

    # ... Continue with the game loop ...

    # Check for pet's life status
    if pet['hunger'] <= 0 or pet['happiness'] <= 0:
        # Play death sound and show RIP sprite
        sounds['death'].play()
        screen.blit(sprites['rip'], (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2))
        pygame.display.update()
        pygame.time.wait(5000)  # Wait for 5 seconds
        running = False  # End game loop

    # Render game
    screen.fill((0, 0, 0))  # Fill screen with black

    # Render pet sprite
    if pet['type'] is not None:
        screen.blit(sprites[pet['type']], (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2))

    # Update the display
    pygame.display.flip()

    # Cap the frame rate
    clock.tick(FPS)

    # In your game loop, you would call this function when the player feeds their pet
    # ...

    # Sprite animation
    current_sprite_frame = 0
    sprite_frames = [sprites[pet_type],
                     pygame.transform.flip(sprites[pet_type], True, False)]  # Assuming left and right frames

    # In the game loop where you draw your pet
    current_sprite_frame = (current_sprite_frame + 1) % len(sprite_frames)
    screen.blit(sprite_frames[current_sprite_frame], pet_position)

    # Update pet_position to make it walk back and forth
    # ...

pygame.quit()
