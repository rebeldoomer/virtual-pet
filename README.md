# Virtual Pet

## Description
~~-Similar to tamagotchi, but with a bigger focus on breeding.~~ (Not yet implemented properly.)

## Visuals
~~-Not yet set up, but sprites are ready for now.~~

## Installation
-N/A, but requires python and installing the libraries found in requirements text file.

## Usage
-N/A

## Support
-N/A for now.
## Roadmap
-Planning to add proper breeding mechanics, add genders beyond male and female (will add intersex, non-binary, etc. pets), add some simple mini games, etc. Many things planned. But is worked on very rarely since I am too busy.

## Contributing
-Feel free to contribute or make your own spinoff.

## Authors and acknowledgment
Sprites: by GamebowGames from itchi.io https://gamebowgames.itch.io/16x16-small-animals-for-use-with-gbstudio

## License
??? N/A for now
## Project status
WIP, very very slowly being worked on. 
